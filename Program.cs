﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace RandomNumberTest
{
    #region Code Documentation

    /// <summary>
    /// Random Number Generator.
    /// This Program is used for generation of grandom numbers and store them in the list.
    /// This program generates a list of 10,000 numbers in random order each time it is run.
    /// Each number in the list is unique and is between 1 and 10,000 (inclusive).
    /// Contains the following methods for populating the list:
    /// <list type="number">
    ///     <item>
    ///         <term>Question</term>
    ///         <description>The question to user is displayed on the screen.</description>
    ///     </item>
    ///     <item>
    ///         <term>AnswerTest</term>
    ///         <description>The user entry is tested for proper input [Y / N ].</description>
    ///     </item>
    ///     <item>
    ///         <term>GenerateList</term>
    ///         <description>The list of randomly generated numbers is created.</description>
    ///     </item>
    ///     <item>
    ///         <term>ListGeneratedMsg</term>
    ///         <description>The message is displayed. The list of random numbers was generated and is displayed on the screen.</description>
    ///     </item>
    ///     <item>
    ///         <term>ListNotGeneratedMsg</term>
    ///         <description>The message is displayed. The list of random numbers was not generated.</description>
    ///     </item>
    ///     <item>
    ///         <term>ListGenratedSortedMsg</term>
    ///         <description>The sorted list of randomly gerated numbers is displayed on the screen.</description>
    ///     </item>
    ///     <item>
    ///         <term>ReturnToFinish</term>
    ///         <description>The message is displayed. User to press RETURN to terminate the program.</description>
    ///     </item>
    /// </list>
    /// </summary>
    /// <remarks>Martin Frena, Sep/16/2019.</remarks>

    #endregion

    class Program
    {
        /// <summary>Defines the entry point of the application.</summary>
        /// <param name="args">The arguments.</param>
        static void Main(string[] args)
        {
            #region Variables, Constants and Objects Declaration

            //Variable - answer to question
            string generateNewList = null;

            //List capacity and random list declaration
            const int capacityList = 10000;
            List<int> rndList = new List<int>(capacityList);

            // Instantiate random number generator. Using "Random.Next(int minValue, int maxValue)" Method  
            Random rnd = new Random();
            const int rndNumberMin = 1;
            const int rndNumberMax = capacityList + 1;

            #endregion

            Console.WriteLine();
            Console.WriteLine("This program generates a list of 10,000 numbers in random order each time it is run.");
            Console.WriteLine("Each number in the list is unique and is between 1 and 10,000 (inclusive).");
            Console.WriteLine();

            #region Random Numbers List Generation

            try // Handling error that occurs during the execution of an application.
            {
                //Do you want to generate a random list question/answer
                generateNewList = Question();

                //Test if the answer was Y / N    
                generateNewList = AnswerTest(generateNewList);

                //Generate/Populate Random List, Display list, Display sorted list and Dispaly messages, when answer was "Y"
                while (generateNewList == "Y" || generateNewList == "y")
                {
                    //Clear the list, remove all items from the list 
                    rndList.Clear();
                    //Generate List of random numbers
                    GenerateList(capacityList, rndList, rnd, rndNumberMin, rndNumberMax);
                    //Display message "The list of random numbers was generated" 
                    ListGeneratedMsg();
                    //Display sorted List of random numbers previously generated
                    ListGenratedSortedMsg(rndList);
                    //Do you want to generate a random list question/answer
                    generateNewList = Question();
                    //Test if the answer was Y / N   
                    generateNewList = AnswerTest(generateNewList);
                }

                //Display message "The list of random numbers was not generated", when answer was "N"
                if (generateNewList == "N" || generateNewList == "n")
                {
                    ListNotGeneratedMsg();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Inside catch block. Exception: {0}", ex.Message);
                Console.WriteLine("Please contact your support team at 1-800-CAL-SPRT");
            }

            #endregion

            //Press return to finish the program
            ReturnToFinish();
        }

        #region Methods and Functions

        /// <summary>The question to user is displayed on the screen.</summary>
        /// <returns>User answer to the prompt displayed on the sceen.</returns>
        private static string Question()
        {
            string answer;
            Console.WriteLine();
            Console.Write("Do you want to generate new list [ Y / N ] : ");
            answer = Console.ReadLine();
            Console.WriteLine();
            return answer;
        }

        /// <summary>The user entry is tested for proper input [Y / N ].</summary>
        /// <param name="generateNewList">User answer is tested for correct entry.</param>
        /// <returns>User answer.</returns>
        private static string AnswerTest(string generateNewList)
        {
            while ((Regex.IsMatch(generateNewList, "^[ny]+$", RegexOptions.IgnoreCase) && generateNewList.Length == 1) == false)
            {
                generateNewList = Question();
            }

            return generateNewList;
        }

        /// <summary>List of random numbers creation.</summary>
        /// <param name="capacityList">List capacity.</param>
        /// <param name="rndList">List object name.</param>
        /// <param name="rnd">Random number generator name</param>
        /// <param name="rndNumberMin">The inclusive lower bound of the random number returned.</param>
        /// <param name="rndNumberMax">The exclusive upper bound of the random number returned. <c>rndNumberMax</c> must be greater than or equal to <c>rndNumberMin</c>.</param>
        private static void GenerateList(int capacityList, IList<int> rndList, Random rnd, int rndNumberMin, int rndNumberMax)
        {
            // Generate/Populate a random list rndList
            while (rndList.Count != capacityList)
            {
                // Generate random number
                int rndNumber = rnd.Next(rndNumberMin, rndNumberMax);

                //Check if the number already exists in the rndList
                if (rndList.Contains(rndNumber) == false)
                {
                    //Add ranndom generated number to the list
                    rndList.Add(rndNumber);
                    //Display generated random number on the screen
                    Console.Write("{0}_", rndNumber);
                }
            }
            Console.WriteLine();
        }

        /// <summary>The message is displayed. The list of random numbers was generated.</summary>
        private static void ListGeneratedMsg()
        {
            Console.WriteLine();
            Console.WriteLine("The list of random numbers was generated.");
            Console.WriteLine();

        }

        /// <summary>The message is displayed. The list of random numbers was not generated.</summary>
        private static void ListNotGeneratedMsg()
        {
            Console.WriteLine();
            Console.WriteLine("The list of random numbers was not generated.");
            Console.WriteLine();

        }

        /// <summary>The sorted list of randomly gerated numbers is displayed on the screen.</summary>
        /// <param name="rndList">List object name.</param>
        private static void ListGenratedSortedMsg(List<int> rndList)
        {
            rndList.Sort();
            foreach (var element in rndList) Console.Write("{0}_", element);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("The sorted random list displayed.");
            Console.WriteLine();
        }

        /// <summary>Press RETURN to finish the program</summary>
        private static void ReturnToFinish()
        {
            Console.WriteLine("Press RETURN to finish the program.");
            Console.ReadLine();
        }

        #endregion

    }
}
