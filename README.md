**List of Random Numbers Generator **

This program generates a list of 10,000 numbers in random order each time it is run.
Each number in the list is unique and is between 1 and 10,000 (inclusive).

